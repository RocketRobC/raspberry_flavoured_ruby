Raspberry Flavoured Ruby
========================

Who here has a raspberry pi? When I tell people I've been playing around with a
raspberry pi they often say they have one but they never use. Well tonight I
wanted to talk about a little project that might inspire people to dig their's
out of the bottom draw or get one to check it out. 

If you are familiar with Raspberry Pi's, Ruby might not be the first language
that springs to mind when it comes to interacting with a Pi. Python is very
common when writing for the Pi and will often have better library support than
other languages. However as rubyists, there are still libraries available that
allow you to interact with features of the pi and that's what I want to show
tonight. 

Before that, let's understand a bit more about the origin of the Raspberry Pi.
The best introduction I found comes from a quote in the Raspberry Pi Foundation
2016 to 2018 strategy document. 

**show quote**

"Raspberry Pi Foundation was established in 2008 as a UK-based charity with the
purpose “to further the advancement of education of adults and children,
particularly in the field of computers, computer science and related subjects”.

Driven by this mission the founders set about creating an inexpensive computer that
would lower the barrier of entry into development. Once the first Raspberry Pi
was released they found they'd also struck a nerve with those already in the
industry and the maker community. The low cost and ease of interconnectivity
launched the Pi to being one of the most used prototyping platforms to date. 

What makes it so popular and one of the key features of the raspberry pi and
other single board computers is the accessibility of the general input output
pins or GPIO. 

**image of pins**

These pins allow you to interface directly with other electronic components to
send or receive signals. You can see in this image, I've got some jumper cables
hooked up. To demonstrate this a good place to begin is a really simple circuit
and script that controls some LED's. 

**show led control script**

To access the library we require it then we need to decide on the numbering
pattern used to identify the pins so here we're using the board setting. I'm
setting some constants that represent the pin used for each LED and then setting
that pin as an output starting low, or off. The switch on method simply controls
the pin you send it for the provided duration.

**chaser scripts**

This script runs the LEDs in a chaser sequence. We're requiring in the
controller class and then there are some methods here to control the order the
LEDs light in. Let's se this one work.

**demo**

You'll notice some other bits and pieces here.

**breadboard**

This is a breadboard that allows you to easily connect components to the GPIO
pins of the Pi. You can see in this the LEDs, resistors and jumpers used to
connect them.

Now we've seen some basic control, what can we do with it? There really are
endless possibilities and the boundaries are being pushed every day but I wanted
to show you a recent project I worked to make life a little easier.

A few months ago my wife, 2 two boys and I moved back in with my parents in law.
I know this sounds a little sad, but It's not all bad. It's pretty nice and we
have our own version of a tiny house above the garage. We have our own space
however I keep some things below down in the garage and need to get in and out
but unfortunately, there weren't enough door openers to go around. So, rather
than just getting another one off eBay for $5, 

**Image of remote**

my first thought was how could I open the door using the Raspberry Pi. 

Turning to Google I started digging into how these remotes work and realised I
could get the parts to do it myself. I headed down to Jaycar I got one of these,
a 433Mhz transmitter module. 

**Image**

433Mhz is the frequency that garage door openers, car keys and other key fob
style remotes use as a carrier frequency.

It has 4 pins, a voltage input, a ground, a data input and an antenna. 

To communicate with the opener, these devices send a digital signal, a pattern
of ons and offs. The signal is sent to the data pin simply by setting it high or
low. The receiver in the door opener listens for a pattern that it knows and
when it hears one, opens or closes the door.

So what signal is needed to open the door? Quite a bit more googling later I
learnt that in the states, the Federal Communications Commission or FCC conducts
tests on all devices that transmit or receive and publish a report of their
findings. This includes key fob remotes. 

**Show site**

https://fccid.io is a repository for FCC reports. It allows you to search by
FCCID or frequency and view the FCC report on that device. It's pretty
interesting, If you look on the back of your phone or computer for example
you'll see an FFCID number. Search for that and you'll get extensive details on
your device. 

I couldn't find an FCCID on the remote I was using so I started searching for
other remotes that looked similar. The reports I downloaded included a 
chart mapping the devices signal clearly showing the highs and lows and the
duration if each. 

**show chart**

In this chart you can see the whole cycle. A zero is represented by a long high
and a short low. A one is the opposite, a short high and a long low. 

**zoomed chart**

The chart zooms in on a region and gives the duration that each is high or low
for. 

It then became a matter of recreating this signal in code and testing it out.

Here's the first script I wrote...

**show script**

It starts with the basic setup, we require rpi_gpio and set the pins to use.
There are also a couple of LEDs used to indicate when it's running. Below that
there's the code we're sending and then the number of cycles we'll send. I've
hardcoded the durations of a long and short value. Then we move to the transmit
method. Start with turning on an LED, then loop through sending the values. For
each value in the code above with either call the send_zero or send_one methods.
After that we turn the green LED off and add a short sleep between each cycle.
Then turn the orange LED off and reset the pins.

send_zero and send_one simply set the pin high or low for the duration set
above. A zero is a long followed by short and the one is the short followed by
long. The reset is just controlling the LEDs and resetting the pins. 

Once done, with the pi upstairs I headed down to the garage to test it out. This
particular opener can be paired with a remote by learning it's
signal so I hit the learn button, ran the script and the door opened. 

It had worked and I was pretty amazed it happened first time. I'd recreated a
signal that could open the garage. Now that was out of the way, why stop there.
What about the front gate.

The gate was a little different, the receiver is in a sealed water tight box
with no learn button to make it easy. To get this to work I'd need to capture the
signal from the remote and play it back. 

Back to Jaycar to get one of these.

**receiver image**

It's a 433MHz receiver with 8 pins. It requires a voltage, ground, antenna and
data signal. With this module there are some doubled up pins, that why there are
eight. It's important to note that this unit will accommodate upto 5v on the
input. The pi does have a 5v power rail but only for supply. The GPIO pins only
take a 3.3v signal so I've only supplied this with 3.3v to make sure I don't
damage the pi with a higher input from the data pin. 

With this setup I wrote a script to capture the signal.

**receive script**

Again we go through the setup, this time I'm also requiring csv as I want to
use that to capture the data. I'm setting a duration here which is the length of
time I want to capture. Into the record method there's a counter here for the
cumulative time and we also set a variable for the start time and establish a
hash to hold the readings. Into the loop, this controls how long it will capture
for. The diff variable captures the timestamp of each reading which then becomes
the key of the data hash. The RPi_GPIO library gives us a method to check the
state of a pin so if it's high we get a true. After this loop we write the CSV
and reset the pins.

In the first version, I was just letting it run as fast as it could. This ended
up with a CSV of about 387920 rows for 5 seconds of recording time. 

**show end of csv**

Data at this resolution was hard to manipulate and also introduced more noise so I
ended up adding a short sleep into the record loop to limit the sample rate and
got it down to about 66000 rows.

With all this data I needed to be able workout what the pattern was. I tried
graphing it using a gem called gruff but there was to much to get a clear image.
Chatting about it at work, one of my colleagues dumped it into Tableau. Doing
this we could clearly see the shape of a signal so I knew the recording had
worked, but I still needed to measure the duration of the highs and lows so I
wrote this script to calculate their duration. 

**measure script**

Here I'm reading through the CSV and writing each timestamp the land at a change
between true and false to an array. Then we take pairs of values from the array,
calculate the difference and write these to a new text file.

**length data text**

This was helpful, you can workout the pattern by the timings. What
I started to notice was this signal wasn't like the previous one that worked on
the garage. This had different sections and different timings, it was more
complex. Each time I read through the new data the pattern changed.

**gate v1 script**

This is the first version of the script to try an open the gate. There are
several commented out codes that I tried. I noticed with this signal there was a
preceding pattern before each cycle that was the same so I tried to recreate it
here in the send_sync method. The code method does the same as the previous
example sending either a zero or one. 

Unfortunately none of this worked. Unlike the garage, it wasn't as easy. 

Taking a different approach I thought, why don't I just play back the data that
was recorded.

I wrote another script to convert the CSV data to a string of ones and zeros
representing the high or low state.

**convert_to_bin**

and this gave me a text file that looked like this.

**white3_bin**

I used this as the source for the code of the second version. Instead of setting
durations for each high and low, I'd just let it send the signal back in the
same way it was captured. 

**gate v2**

This is version 2. Here it just reads the file and the transmit method is a lot
neater. The sleep in the code method is the same duration as the one used when
capturing. This ensures it's sent at the same rate it was captured. 

The first time I ran this it didn't seem to work, but then it did. Testing with
new data captured from the remote, I got the gate to open, but only once for
each new set of captured data. I realised this might be something else I read
about, a rolling code. This is a security measure used to prevent people doing
what I'm trying to do. Trying to crack the security just to get the gate to open
was a little beyond what I's set out to achieve so I was happy to let it rest at
this point. 

To finish up I'll ask, was all of this worth it? There was lot of effort for a
pretty small result. But I can say yes, it was. It's been a great way to
introduce myself to the basics of the Raspberry Pi and set a few small coding
challenges along the way. I'll continue to explore code through the Pi and I
already have a list of projects in the queue. When the code you write extend
beyond the terminal or the browser into the physical world, I think it's a
pretty amazing experience. 

