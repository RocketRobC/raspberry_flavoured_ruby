Raspberry Flavoured Ruby
========================

### Raspberry Pi basics using Ruby

Note:
Who here has a raspberry pi? Cool. Who doesn't know what a Raspberry Pi is? For
those that don't I hope to shed a little light on how cool this tiny computer is. 

When I tell people I've been playing around with a raspberry pi they often say
they have one but never use it. Well tonight I wanted to talk about a little
project that might inspire you to dig your's out of the bottom draw or even go
and get one. 

---

**Raspberry Pi Model 3**

![Pi3](assets/pi_model_3.jpeg)

**SoC:** Broadcom BCM2837

1.2GHz Quad ARM Cortex-A53, 1GB DDR2 RAM

Note: If you are familiar with Raspberry Pi's, Ruby might not be the first
language that springs to mind when it comes to interacting with it's features.
Python is very common and will often have better library support than other
languages. However as Rubyists, there are still libraries available that allow
you to interact with the Pi and that's what I'll show tonight. 

Before that, let's understand a bit more about it's origin. The best
introduction I found comes from a quote in the Raspberry Pi Foundation 2016 to
2018 strategy document. 

---

"Raspberry Pi Foundation was established in 2008 as a UK-based charity with the
purpose to further the advancement of education of adults and children,
particularly in the field of computers, computer science and related subjects”.

Note:
Driven by this mission the founders set about creating an inexpensive computer that
would lower the barrier of entry into development. Once the first Raspberry Pi
was released they found they'd also struck a nerve with those already in the
industry and the maker community. The low cost and ease of interconnectivity
launched the Pi to being one of the most used prototyping platforms to date. 

What makes it so popular and one of it's key features is the accessibility of
the general input output pins or GPIO. 

---

![GPIO Pins](assets/gpio_pins.jpeg)

Note: These pins allow you to interface directly with other electronic
components to send or receive signals. To demonstrate this a good place to begin
is a really simple circuit and script that controls some LED's. 

---
```ruby
require 'rpi_gpio'

class LedControl
  RPi::GPIO.set_numbering :board

  RED = 11
  ORANGE = 12
  YELLOW = 13
  GREEN = 15

  RPi::GPIO.setup RED, as: :output, initialize: :low
  RPi::GPIO.setup ORANGE, as: :output, initialize: :low
  RPi::GPIO.setup YELLOW, as: :output, initialize: :low
  RPi::GPIO.setup GREEN, as: :output, initialize: :low

  def switch_on(led, duration = nil)
    led_on(led)
    duration ? sleep(duration) : nil
    led_off(led)
  end

  def led_collection
    [RED, ORANGE, YELLOW, GREEN]
  end

  def random_selection
    led_collection.sample
  end

  def gpio_reset
    RPi::GPIO.reset
  end

  private

  def led_on(pin)
    RPi::GPIO.set_high(pin)
  end

  def led_off(pin)
    RPi::GPIO.set_low(pin)
  end
end
```

Note: To control the GPIO using Ruby I'm using the RPi_GPIO library, so we
require that in. Then we need to decide on the numbering pattern used to
identify the pins so here we're using the board setting. I'm setting some
constants that represent the pin used for each LED and then setting that pin as
an output starting low, or off. The switch on method simply controls the pin you
send it for the provided duration.

---
```ruby
require_relative 'led_control'

class Chaser

  def initialize
    @led = LedControl.new
  end

  def seq1(reps)
    reps.times { seq(@led.led_collection, 0.03) }
  end

  def seq2(reps)
    collection_size = @led.led_collection.size
    forward = @led.led_collection.first(collection_size - 1)
    backward = @led.led_collection.last(collection_size - 1).reverse
    pattern = forward + backward
    reps.times { seq(pattern, 0.05) }
  end

  def seq3(reps)
    reps.times { seq(@led.led_collection.reverse, 0.03) }
  end

  def gpio_reset
    @led.gpio_reset
  end

  private

  def seq(order, duration)
    order.each do |led|
      @led.switch_on(led, duration)
      sleep(duration)
    end
  end

end

chaser = Chaser.new
chaser.seq2(4)
chaser.seq1(2)
chaser.seq3(2)
chaser.seq1(2)
chaser.seq3(2)
chaser.seq2(4)
chaser.seq1(2)
chaser.seq3(2)
chaser.seq1(2)
chaser.seq3(2)
chaser.seq2(3)
chaser.seq2(3)
chaser.gpio_reset
```

Note:
This script runs the LEDs in a chaser sequence. We're requiring in the
controller class and then there are some methods to light the LEDs in different
patterns. Let's see this one in action.

---

![Breadboard](assets/breadboard.jpeg)

Note:
You'll notice some other bit and pieces here.

This is a breadboard that allows you to really easily connect components to the GPIO
pins of the Pi.

---

### Now What?

Note:
Now we've seen some basic control, what can we do with it? There really are
endless possibilities and the boundaries are being pushed every day but I wanted
to show you a recent project I worked to make life a little easier.

A few months ago my wife, 2 two boys and I moved back in with the in-laws.
I know this sounds a little sad, but It's not all bad. We have our own space
however I do keep some things down in the garage and need to get in and out
but unfortunately, there weren't enough door openers to go around. So, rather
than just getting another one of these 

---

![Remote](assets/remote.jpeg)

Note:
off eBay for $5. My first thought was how could I use the Raspberry Pi to open
the door?

Turning to Google I started digging into how these remotes worked and realised
it was easy to get the parts to do it myself. I headed down to Jaycar to get one
of these, a 433Mhz transmitter module. 

---

![433MHz Transmitter](assets/transmitter.jpg)

Note:
433Mhz is the frequency that garage door openers, car keys and other key fob
style remotes use as a carrier frequency.

This one has 4 pins, a voltage input, a ground, a data input and an antenna.

To communicate with the opener, these devices send a digital signal, a pattern
of ons and offs. The signal is sent to the data pin simply by setting it high or
low. The receiver in the door opener listens for a pattern that it knows and
when it hears one, opens or closes the door.

So what signal is needed to open the door? Quite a bit more googling later I
learnt that in the states, the Federal Communications Commission or FCC conducts
tests on all devices that transmit or receive this includes key fob remotes.

---

![FCCID Website](assets/fccid.png)

https://fccid.io

Note: https://fccid.io. is a repository for FCC reports. It allows you to search
by FCCID and view the report for that device. It's pretty interesting, if you
look on the back of your phone or computer for example you'll see an FFCID
number. Search for that and you'll get extensive details on your device.

I couldn't find an FCCID on the remote I was using so I started searching for
other remotes that looked similar. The reports I downloaded included a
chart mapping the devices signal clearly showing the shape of the signal.

---

![The whole signal](assets/signal_graph_1.png)

Note:
In this chart you can see the whole cycle. A zero is represented by a long high
and a short low. A one is the opposite, a short high and a long low.

---

![Data length](assets/signal_graph_2.png)

Note:
The chart zooms in on a region and gives the duration that each is high or low
for.

It then became a matter of recreating this signal in code and testing it out.

Here's the first script I wrote...

---?code=lib/open.rb

Note:
It starts with the basic setup, we require rpi_gpio and set the pins to use.
There are also a couple of LEDs used to indicate when it's running. Below that
there's the code we're sending and then the number of cycles we'll send. I've
hardcoded the durations of a long and short value. Then we move to the transmit
method. Start with turning on an LED, then loop through sending the values. For
each value in the code we either call the send_zero or send_one methods.
After that we turn the green LED off and add a short sleep between each cycle.
Then turn the orange LED off and reset the pins.

send_zero and send_one simply set the pin high or low for the duration set
above. A zero is a long followed by short and the one is the short followed by
long.

---

### Time to test it out

Note:
Once done, with the pi upstairs I headed down to the garage to test it out. This
particular opener can be paired with a remote by learning it's
signal so I hit the learn button, ran the script and the door opened. 

It had worked and I was pretty amazed it happened first time. I'd recreated a
signal that could open the garage. Now that was out of the way, why stop there.

---

### What about the front gate.

Note:
The gate was a little different, the receiver is in a sealed water tight box
with no learn button to make it easy. To get this to work I'd need to capture the
signal from the remote and play it back. 

Back to Jaycar to get one of these.

---

![433MHz receiver](assets/receiver.jpg)

Note:
It's a 433MHz receiver with 8 pins. It requires a voltage, ground, antenna and
data signal. With this module there are some doubled up pins, that why there are
eight. 

With this setup I wrote a script to capture the signal.

---?code=lib/receive2.rb

Note:
Again we go through the setup, this time I'm also requiring csv as I want to
use that to capture the data. I'm setting a duration here which is the length of
time I want to capture. Into the record method there's a counter here for the
cumulative time and we also set a variable for the start time and establish a
hash to hold the readings. Into the loop, this controls how long it will capture
for. The diff variable captures the timestamp of each reading which then becomes
the key of the data hash. The RPi_GPIO library gives us a method to check the
state of a pin so if it's high we get a true. After this loop we write the CSV
and reset the pins.

---

![CSV Size](assets/csv_size.png)

Note:
With all this data I needed to be able workout what the pattern was. I tried
graphing it using a gem called gruff but there was to much to get a clear image.
Chatting about it at work, one of my colleagues dumped it into Tableau. Doing
this we could clearly see the shape of a signal so I knew the recording had
worked, but I still needed to measure the duration of the highs and lows so I
wrote this script to work it out.

---?code=lib/measure.rb

Note:
Here I'm reading through the CSV and writing each timestamp that land at a change
between true and false to an array. Then we take pairs of values from the array,
calculate the difference and write these to a new text file.

---

![Length Data](assets/length_data.png)

Note: This was helpful, you can workout the pattern by the timings. What
I started to notice was this signal wasn't like the previous one that worked on
the garage. This had a different structure with different timings, it was more
complex.

---?code=lib/gate.rb

Note:
This is the first version of the script to try an open the gate. There are
several commented out codes that I tried. I noticed with this signal there was a
preceding pattern before each cycle that was the same so I tried to recreate it
here in the send_sync method. The code method does the same as the previous
example sending either a zero or one.

Unfortunately none of this worked. So taking a different approach I thought, why
don't I just send back the data that was recorded.

---?code=lib/convert_to_bin.rb

Note:
I wrote another script to convert the CSV data to a string of ones and zeros
representing the high or low state.

and this gave me a text file that looked like this.

---

<span style='font-size:20px'>0000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000011111000011111000011110000111110001111
1000011110000111110000111110000111100001111000011111000011111000011110000111100
0011111000011110000000000000000000000000000000000000000000111111111000111111111
0000111100000000011111111100011111000000001111111110000111100000000011110000000
0011110000000011111111100001111000000000111111110000111110000000111110000000011
1110000000011110000000001111111110000111100000000111100000000011111111100001111
0000000011111111100001111000000000111100000000111110000000011111111100001111111
1100001111000000001111111110000111111111000011111111000011111111100001111100000
0001111000000000111100000000111111111000011110000000001111111100001111111110000
1111111110001111111110000111110000000011110000000001111111100001111100000000111
1111110001111111110000111110000000011111111000011111111100001111111110001111111
1100001111111110000111111110000111111111000011111111100001111111100001111111110
0001111111100001111111110000111111111000011111111000011111000000001111111100001
1111111100001111100000000000000000000000000000000000000000000000000000000000000
0000000011111000111110000111110000111000011111000011111000011110000111110000111
1100001111000011111000011111000111110000111110000111000011111000000000000000000
0000000000000000000000000111111111000011111111100001111000000000111111110001111</span>

Note:
I used this as the source for the code of the second version. 

---?code=lib/gate_2.rb

Note:
This is version 2. Here it just reads the file and the transmit method is a lot
neater. 

The first time I ran this it didn't seem to work, but then it did. Testing with
new data captured from the remote, I got the gate to open, but only once for
each new set of captured data. I realised this might be something else I read
about, a rolling code. This is a security measure used to prevent people doing
pretty much what I'm trying to do. Trying to crack this just to get the gate to
open was a little beyond what I set out to achieve so I was happy to let it rest
at this point.

---

### Was it worth it?

Note:
So was all of this worth it? There was lot of effort for a
pretty small result. But I can say yes

---

### Was it worth it?
# YES!

Note:
it was. It's been a great way to
introduce myself to the basics of the Raspberry Pi and set a few small coding
challenges along the way. I'll continue to explore code through the Pi and I
already have a list of projects in the backlog. When the code you write extend
beyond the terminal or the browser into the physical world, I think it's a
pretty amazing experience.

---

## Thanks for listening
<br/>
### Resources
https://www.raspberrypi.org

**RPi-GPIO** https://github.com/ClockVapor/rpi_gpio

**Pi Piper** https://github.com/jwhitehorn/pi_piper
<br/>
<br/>
twitter: @robcornish
