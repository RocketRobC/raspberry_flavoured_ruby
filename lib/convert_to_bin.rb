require 'csv'

in_file, out_file = ARGV

data = CSV.read("#{in_file}.csv")
output = File.open("#{out_file}.txt", 'wb')
data.each do |_, value|
  output << if value == 'true'
              1
            else
              0
            end
end
output.close
