require 'rpi_gpio'

class Transmission
  RPi::GPIO.set_numbering :board

  SIGNAL = 12

  ORANGE = 29
  GREEN = 32 

  RPi::GPIO.setup SIGNAL, as: :output, initialize: :low
  RPi::GPIO.setup ORANGE, as: :output, initialize: :low
  RPi::GPIO.setup GREEN, as: :output, initialize: :low

  CODE = File.open('/home/pi/projects/pi_garage_door/black1_bin.txt', 'r')
  CYCLE = 1

  def transmit
    led_on(ORANGE)
    CYCLE.times do
      code
    end
    led_off(ORANGE)
    gpio_reset
  end
  
  def code
    CODE.each_char do |n|
      n == '0' ? send_zero : send_one
      sleep(0.00001)
    end
  end

  def send_zero
    led_off(GREEN)
    RPi::GPIO.set_low(SIGNAL)
  end

  def send_one
    led_on(GREEN)
    RPi::GPIO.set_high(SIGNAL)
  end

  def led_on(colour)
    RPi::GPIO.set_high(colour)
  end

  def led_off(colour)
    RPi::GPIO.set_low(colour)
  end

  def gpio_reset
    RPi::GPIO.reset
  end
end

Transmission.new.transmit
