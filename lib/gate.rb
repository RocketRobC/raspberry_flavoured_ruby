require 'rpi_gpio'

class Transmission
  RPi::GPIO.set_numbering :board

  SIGNAL = 12

  ORANGE = 29
  GREEN = 32 

  RPi::GPIO.setup SIGNAL, as: :output, initialize: :low
  RPi::GPIO.setup ORANGE, as: :output, initialize: :low
  RPi::GPIO.setup GREEN, as: :output, initialize: :low

  # CODE = '101010110001001001100111000101000110001101100101010000000000001001000'
  # CODE = '000000101100011110111111101111110110001101100101010000000000001000000'
  # CODE = '000010010100111111100111110111100110001101100101010000000000001001100'
  
  # CODE = '100000011001100110010000110010010110001101100101010000000000001000000'
  CODE = '10000001100110011001000011001001011000110110010101000000000000100000'
  #
  # CODE = '011111100110011001101111001101101001110010011010101111111111110111111'
  # CODE = '0110001101100101010000000000001001100'
  CYCLE = 40
  SYNC_LENGTH = 15

  # LONG = 0.000809
  LONG = 0.000783
  # SHORT = 0.000409
  SHORT = 0.000422
  SYNC_SHORT = 0.00035

  def transmit
    led_on(ORANGE)
    CYCLE.times do
      led_on(GREEN)
      send_sync
      RPi::GPIO.set_high(SIGNAL)
      sleep(SYNC_SHORT)
      RPi::GPIO.set_low(SIGNAL)
      led_off(GREEN)
      sleep(0.00396)
      led_on(GREEN)
      code
      RPi::GPIO.set_high(SIGNAL)
      sleep(LONG)
      RPi::GPIO.set_low(SIGNAL)
      led_off(GREEN)
      sleep(0.0208)
    end
    led_off(ORANGE)
    gpio_reset
  end
  
  def sync
    SYNC_LENGTH.times { send_sync }
  end

  def code
    CODE.each_char do |n|
      n == '0' ? send_zero : send_one
    end
  end

  def send_sync
    RPi::GPIO.set_high(SIGNAL)
    sleep(SHORT)
    RPi::GPIO.set_low(SIGNAL)
    sleep(SYNC_SHORT)
  end


  def send_zero
    RPi::GPIO.set_high(SIGNAL)
    sleep(LONG)
    RPi::GPIO.set_low(SIGNAL)
    sleep(SHORT)
  end

  def send_one
    RPi::GPIO.set_high(SIGNAL)
    sleep(SHORT)
    RPi::GPIO.set_low(SIGNAL)
    sleep(LONG)
  end

  def led_on(colour)
    RPi::GPIO.set_high(colour)
  end

  def led_off(colour)
    RPi::GPIO.set_low(colour)
  end

  def gpio_reset
    RPi::GPIO.reset
  end
end

Transmission.new.transmit
