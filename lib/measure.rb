require 'csv'

change_times = []
prev_val = false
data = CSV.read('gate_white_1.csv')
data.each do |time, value|
  unless prev_val == value
    change_times << time
    prev_val = value
  end
end

results = [0.0]
change_times.each_cons(2) do |t1, t2|
  diff = t2.to_f - t1.to_f
  results << diff
end

output = File.open('length_data_6.txt', 'wb')
results.each { |r| output << r.to_s + "\n" }
output.close

binary = []
results.each_slice(2) do |r1, r2|
  next if r2.nil?
  binary << if !(0.00035..0.00085).cover?(r1) && !(0.00035..0.00085).cover?(r2)
              'nil'
            elsif r1 > r2
              0
            else
              1
            end
end

bin_output = File.open('white4_bin.txt', 'wb')
binary.each { |b| bin_output << b }
bin_output.close
