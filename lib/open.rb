require 'rpi_gpio'

class Transmission
  RPi::GPIO.set_numbering :board

  SIGNAL =12

  ORANGE = 29
  GREEN = 32 

  RPi::GPIO.setup SIGNAL, as: :output, initialize: :low
  RPi::GPIO.setup ORANGE, as: :output, initialize: :low
  RPi::GPIO.setup GREEN, as: :output, initialize: :low

  CODE = '0011100101001110001010111'
  CYCLE = 20

  LONG = 0.001025
  SHORT = 0.000325

  def transmit
    led_on(ORANGE)
    CYCLE.times do
      led_on(GREEN)
      CODE.each_char do |n|
        n == '0' ? send_zero : send_one
      end
      led_off(GREEN)
      sleep(0.0096)
    end
    led_off(ORANGE)
    gpio_reset
  end

  def send_zero
    RPi::GPIO.set_high(SIGNAL)
    sleep(LONG)
    RPi::GPIO.set_low(SIGNAL)
    sleep(SHORT)
  end

  def send_one
    RPi::GPIO.set_high(SIGNAL)
    sleep(SHORT)
    RPi::GPIO.set_low(SIGNAL)
    sleep(LONG)
  end

  def led_on(colour)
    RPi::GPIO.set_high(colour)
  end

  def led_off(colour)
    RPi::GPIO.set_low(colour)
  end

  def gpio_reset
    RPi::GPIO.reset
  end
end

Transmission.new.transmit
