require 'rpi_gpio'
require 'csv'

out_file = ARGV

class Receive
  RPi::GPIO.set_numbering :board

  INPUT = 11

  ORANGE = 29
  GREEN = 32

  RPi::GPIO.setup INPUT, as: :input
  RPi::GPIO.setup ORANGE, as: :output, initialize: :low
  RPi::GPIO.setup GREEN, as: :output, initialize: :low

  DURATION = 6

  def initialize(out_file)
    @out_file = out_file[0]
  end

  def record
    led_on(ORANGE)
    cumulative_time = 0
    start_time = Time.now
    data = {}
    while cumulative_time < DURATION
      diff = Time.now - start_time
      data[diff] = RPi::GPIO.high? INPUT
      cumulative_time = diff
      sleep(0.00001)
    end
    led_off(ORANGE)
    csv(data)
    gpio_reset
  end

  def csv(data)
    led_on(GREEN)
    CSV.open("#{@out_file}.csv", 'wb') do |csv|
      csv << %w(Time Reading)
      data.each do |sample|
        csv << sample
      end
    end
    led_off(ORANGE)
  end

  def led_on(colour)
    RPi::GPIO.set_high(colour)
  end

  def led_off(colour)
    RPi::GPIO.set_low(colour)
  end

  def gpio_reset
    RPi::GPIO.reset
  end
end

Receive.new(out_file).record
